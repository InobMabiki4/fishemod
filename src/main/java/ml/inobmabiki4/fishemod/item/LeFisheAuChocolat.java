package ml.inobmabiki4.fishemod.item;

import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;

public class LeFisheAuChocolat extends Item {

    public LeFisheAuChocolat() {
        super(new Item.Properties()
                .group(ItemGroup.FOOD)
                .food(new Food.Builder()
                        .setAlwaysEdible()
                        .hunger(6)
                        .saturation(7.2F)
                        .effect(() -> new EffectInstance(Effects.REGENERATION, 40), 0.4F)
                        .effect(() -> new EffectInstance(Effects.NAUSEA, 200), 0.3F)
                        .effect(() -> new EffectInstance(Effects.HUNGER, 80), 0.2F)
                        .build()));
    }
}
